# Shuttlecock Labeling Tool
###### tags: `CoachAI`
## Package


- Download link: https://drive.google.com/drive/folders/1MXGVyg95LfUO2WNAb8zluTKg0sbg850Q?usp=sharing
- python3
- numpy
    ```pip3 install numpy```
- opencv2
    ```pip3 install opencv-python```

## Command 
```dockerfile=
python label_tool.py *_**_**.mp4
```

- Put the video which you want to label in  same folder <br>

**If you have predict result of TrackNetV2, you can use label_toolV2.py**
- python label_toolV2.py x_xx_xx.mp4

## Usage

### 1. Label
If the shottlecock is visible, click the center of shuttlecock by left mouse button.

### 2. Keyboard Shortcut

**Please always remember to save .pkl file (by pause "s")**

- **left mouse button** = mark coordinate of shuttlecock's center
- **w** = cancel mark
- **d** = jump to next frame
- **e** = jump to last frame
- **s** = save as .pkl file
- **q** = Exit

## Label Rules
- Please mark the center of shuttlecock 

## Output
- When you press 'q', the tool will be closed. And it will generate file: 
    - * _ ** _ ** _ ball.csv
    - The output csv will look like:
      ![](https://i.imgur.com/AXLMfAD.png)
      <br>


      
      - **Frame** represent the frame of video. 
      - **Visibility** means the shuttlecock is visible or not at this frame(0 : invisible, 1 : visible).
      -  **X**, **Y** is the coordinate of shuttlecock.If shuttlecock is invisible now, then X, Y is 0.
      
<br>


